# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from . import news

__all__ = ['register']


def register():
    Pool.register(
        news.News,
        module='nodo', type_='model')
    Pool.register(
        module='nodo', type_='wizard')
    Pool.register(
        module='nodo', type_='report')
