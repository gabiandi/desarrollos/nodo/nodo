#!/usr/bin/env python3

"""Ayuda a crear la configuración inicial de un entorno de Tryton."""
import datetime
from decimal import Decimal
from proteus import Model, Wizard
from proteus import config
from trytond.modules.country.scripts import import_countries
from trytond.modules.currency.scripts import import_currencies


# Configuración
TRYTON_MODULE_NAME = 'nodo'
TRYTON_COMPANY_NAME = 'Nodo Brote Nativo'
TRYTON_COMPANY_PHONE = '+54 9 345 401-4944'
TRYTON_COMPANY_EMAIL = 'nodo@gmail.com'
TRYTON_COMPANY_WEBSITE = 'www.nodo.com.ar'


def activate_module(tryton_module: str):
    """Activa módulos especificos de Tryton."""
    Module = Model.get('ir.module')
    ConfigWizardItem = Model.get('ir.module.config_wizard.item')

    module, = Module.find([('name', '=', tryton_module)])

    if module.state == 'activated':
        module.click('upgrade')
    else:
        module.click('activate')

    Wizard('ir.module.activate_upgrade').execute('upgrade')

    for item in ConfigWizardItem.find([('state', '!=', 'done')]):
        item.state = 'done'
        item.save()


def setup_country():
    """Configura los paises."""
    import_countries.do_import()


def setup_currency():
    """Configura las monedas."""
    import_currencies.do_import()


def setup_environ(
    company_name: str, company_phone: str, company_email: str, company_website: str
):
    """Configura la compañia."""
    # Idioma
    Lang = Model.get('ir.lang')
    lang_es, = Lang.find([('code', '=', 'es')])

    # Moneda
    Currency = Model.get('currency.currency')
    currency_ars, = Currency.find([('code', '=', 'ARS')])
    currency_ars_rate = currency_ars.rates.new()
    currency_ars_rate.date = datetime.date(datetime.date.today().year, 1, 1)
    currency_ars_rate.rate = Decimal('2')
    currency_ars.save()

    # Tercero
    Party = Model.get('party.party')
    party = Party(name=company_name)
    party.lang = lang_es
    party.phone = company_phone
    party.email = company_email
    party.website = company_website
    party.save()

    # Compañia
    company_config = Wizard('company.company.config')
    company_config.execute('company')
    company = company_config.form
    company.party = party
    company.currency = currency_ars
    company.timezone = 'America/Argentina/Buenos_Aires'
    company_config.execute('add')

    # Usuario admin
    User = Model.get('res.user')
    admin_user, = User.find(['login', '=', 'admin'])
    admin_user.language = lang_es
    admin_user.save()


def setup_purchase():
    """Configura el módulo de compras."""
    PurchaseConfiguration = Model.get('purchase.configuration')
    purchase_configuration = PurchaseConfiguration(1)
    purchase_configuration.purchase_invoice_method = 'manual'
    purchase_configuration.save()


def setup_sale():
    """Configura el módulo de ventas."""
    SaleConfiguration = Model.get('sale.configuration')
    sale_configuration = SaleConfiguration(1)
    sale_configuration.sale_invoice_method = 'manual'
    sale_configuration.sale_shipment_method = 'manual'
    sale_configuration.save()


def setup_custom_module(tryton_config: config.TrytondConfig):
    """Configura el módulo custom."""
    User = Model.get('res.user')

    with tryton_config.set_context(User.get_preferences(True, {})):
        pass


def main():
    print('Iniciando configuración de Tryton')
    tryton_config = config.set_trytond()
    print('Activando módulo de Tryton')
    activate_module(TRYTON_MODULE_NAME)
    print('Configurando paises')
    setup_country()
    print('Configurando monedas')
    setup_currency()
    print('Configurando compañia')
    setup_environ(
        TRYTON_COMPANY_NAME, TRYTON_COMPANY_PHONE, TRYTON_COMPANY_EMAIL, TRYTON_COMPANY_WEBSITE)
    print('Configurando módulo de compras')
    setup_purchase()
    print('Configurando módulo de ventas')
    setup_sale()
    print('Configurando módulo custom')
    setup_custom_module(tryton_config)


if __name__ == '__main__':
    main()
