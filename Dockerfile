FROM tryton/tryton:7.0-office

USER root

COPY . /nodo/tryton/nodo
RUN pip3 install -U /nodo/tryton/nodo --break-system-packages

USER trytond
