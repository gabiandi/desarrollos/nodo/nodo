#!/usr/bin/env python3
# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

"""Setup."""
import os
import io
import re
from pathlib import Path
from configparser import ConfigParser
from setuptools import find_packages, setup


MODULE_NAME = 'trytond_nodo'


def read(fname):
    """Read."""
    with io.open(str(Path(__file__).parent / fname), 'r', encoding='utf-8') as fstream:
        content = fstream.read()
    content = re.sub(r'(?m)^\.\. toctree::\r?\n((^$|^\s.*$)\r?\n)*', '', content)
    return content


def get_require_version(name):
    """Get require version."""
    if minor_version % 2:
        require = '%s >= %s.%s.dev0, < %s.%s'
    else:
        require = '%s >= %s.%s, < %s.%s'
    require %= (name, major_version, minor_version, major_version, minor_version + 1)
    return require


config = ConfigParser()
with open(str(Path(__file__).parent / 'tryton.cfg'), encoding='utf-8') as cstream:
    config.read_file(cstream)
info = dict(config.items('tryton'))
for key in ('depends', 'extras_depend', 'xml'):
    if key in info:
        info[key] = info[key].strip().splitlines()
version = info['version']
major_version, minor_version, _ = version.split('.', 2)
major_version = int(major_version)
minor_version = int(minor_version)
download_url = f'http://downloads.tryton.org/{major_version}.{minor_version}/'
if minor_version % 2:
    version = f'{major_version}.{minor_version}.dev0'
    download_url = f'hg+http://hg.tryton.org/modules/{MODULE_NAME[8:]}#egg={MODULE_NAME}-{version}'
local_version = []
if os.environ.get('CI_JOB_ID'):
    local_version.append(os.environ['CI_JOB_ID'])
else:
    for build in ['CI_BUILD_NUMBER', 'CI_JOB_NUMBER']:
        if os.environ.get(build):
            local_version.append(os.environ[build])
        else:
            local_version = []
            break
if local_version:
    version += '+' + '.'.join(local_version)

requires = [
    'progressbar',
    f'proteus=={version}',
]
tests_require = []
dependency_links = []

for dep in info.get('depends', []):
    if not re.match(r'(ir|res)(\W|$)', dep):
        requires.append(get_require_version(f'trytond_{dep}'))
requires.append(get_require_version('trytond'))
if minor_version % 2:
    dependency_links.append(
        'https://trydevpi.tryton.org/?local_version=' + '.'.join(local_version)
    )

setup(
    name=MODULE_NAME,
    version=version,
    description='',
    long_description=read('README.rst'),
    author='Gabriel Andrés Aguirre',
    author_email='gabiandiagui@gmail.com',
    url='http://www.tryton.org/',
    download_url=download_url,
    project_urls={
        "Bug Tracker": 'https://bugs.tryton.org/',
        "Documentation": 'https://docs.tryton.org/projects/modules-nodo',
        "Forum": 'https://www.tryton.org/forum',
        "Source Code": 'https://hg.tryton.org/modules/nodo',
        },
    keywords='',
    package_dir={'trytond.modules.nodo': '.'},
    packages=(
        ['trytond.modules.nodo']
        + [f'trytond.modules.nodo.{p}' for p in find_packages()]
    ),
    package_data={
        'trytond.modules.nodo': (info.get('xml', [])
            + [
                'tryton.cfg', 'view/*.xml', 'locale/*.po', '*.fodt',
                'icons/*.svg', 'tests/*.rst', 'scripts/*.csv',
            ]),
        },
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: Plugins',
        'Framework :: Tryton',
        'Intended Audience :: Developers',
        'Intended Audience :: Financial and Insurance Industry',
        'Intended Audience :: Legal Industry',
        'License :: OSI Approved :: '
        'GNU General Public License v3 or later (GPLv3+)',
        'Natural Language :: Spanish',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.11',
        'Programming Language :: Python :: Implementation :: CPython',
        'Topic :: Office/Business',
    ],
    license='GPL-3',
    python_requires='>=3.11',
    install_requires=requires,
    extras_require={
        'test': tests_require,
    },
    dependency_links=dependency_links,
    zip_safe=False,
    entry_points="""
    [trytond.modules]
    nodo = trytond.modules.nodo
    [console_scripts]
    trytond_nodo_setup_scenario = trytond.modules.nodo.scripts.setup_scenario:main
    """,  # noqa: E501
)
