# This file is part of Tryton.The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

"""Test modulo."""
from trytond.tests.test_tryton import ModuleTestCase


class NodoTestCase(ModuleTestCase):
    """Test nodo module"""
    module = 'nodo'


del ModuleTestCase
