"""Modelo para publicar noticias."""
from datetime import datetime
from trytond.model import ModelSQL, ModelView, fields, DeactivableMixin


class News(ModelSQL, ModelView, DeactivableMixin):
    """News"""
    __name__ = 'nodo.news'
    _order_name = 'publish_date'

    title = fields.Char('Título', help='Título de la noticia', required=True)
    url = fields.Char('URL', help='URL de la noticia')
    image = fields.Binary('Imagen', help='Imágen de la noticia')
    publish_date = fields.DateTime(
        'Fecha de publicación', help='Fecha de publicación', readonly=True
    )

    @classmethod
    def __setup__(cls):
        super(News, cls).__setup__()
        cls._order.insert(0, ('publish_date', 'DESC NULLS LAST'))

    @classmethod
    def default_publish_date(cls):
        return datetime.now()
